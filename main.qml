import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2

ApplicationWindow {
    id: root
    title: qsTr("Hello World")
    width: 640
    height: 640
    visible: true
    minimumHeight: height
    minimumWidth: width
    maximumHeight: minimumHeight
    maximumWidth: minimumWidth

    ListModel {
        id: horizontalModel
        ListElement {
            page: "qrc:/content/chess.qml"
        }
    }

    ListView {
        id: horizontalView
        model: horizontalModel
        anchors.fill: parent
        interactive: false

        snapMode: ListView.SnapToItem
        orientation: ListView.Horizontal
        highlightMoveDuration: 400

        delegate: Loader {
            id: wrapper
            height: root.height
            width: root.width
            source: page

            ListView.onRemove:
                SequentialAnimation {
                    PropertyAction { target: wrapper; property: "ListView.delayRemove"; value: true }
                    NumberAnimation { target: wrapper; duration: 500 }
                    PropertyAction { target: wrapper; property: "ListView.delayRemove"; value: false }
                }
        }
    }
}
