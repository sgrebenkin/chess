#ifndef BOARDMODEL_H
#define BOARDMODEL_H

#include <QObject>
#include <QVariant>
#include <QAbstractListModel>

enum PieceType {
    NONE, PAWN, ROOK, KNIGHT, BISHOP, QUEEN, KING,
};

class Piece {
    bool d_color;
    PieceType d_type;
    bool d_moved;
public:
    Piece() : d_color(true), d_type(NONE), d_moved(false) {}
    Piece(bool c, PieceType t) : d_color(c), d_type(t), d_moved(false) {}
    PieceType type() const { return d_type; }
    bool color() const { return d_color; }
    void setMoved() { d_moved = true; }
    bool moved() const { return d_moved; }
};

namespace Knight {
    template <class F>
    void variants(int pos, F f) {
        int x = pos % 8;
        int y = pos / 8;

        for (auto mx: {-2, -1, 1, 2})
            for (auto my: {-2, -1, 1, 2})
                if (abs(mx) != abs(my)) {
                    int nx = x + mx;
                    int ny = y + my;
                    if (nx >= 0 && nx < 8 && ny >= 0 && ny < 8)
                        f(ny * 8 + nx);
                }
    }

    template <class S, class A>
    void steps(int pos, bool color, const std::vector<Piece>& board, S step, A attack) {
        variants(pos, [](int s) {
            if (board[s].type() == NONE)
                step(s);
            else (board[s].color() != color)
                attack(s);
        });
        return res;
    }
}

namespace Bishop {
    template <class F>
    void variants(int pos, F f) {
        int x = pos % 8;
        int y = pos / 8;

        int mx = 0;
        int my = 0;

        std::function<bool()> func = [&] () {
            int nx = x + mx;
            int ny = y + my;
            if (nx >= 0 && nx < 8 && ny >= 0 && ny < 8)
                return f(ny * 8 + nx);
        };

        mx = x; my = y;
        while (--mx > 0 && --my > 0 && func());

        mx = x; my = y;
        while (++mx < 8 && --my > 0 && func());

        mx = x; my = y;
        while (--mx > 0 && ++my < 8 && func());

        mx = x; my = y;
        while (++mx < 8 && ++my < 8 && func());
    }

    template <class S, class A>
    void steps(int pos, bool color, const std::vector<Piece>& board, S step, A attack) {
        variants(pos, [](int s) {
            if (board[s].type() == NONE) {
                step(s);
                return true;
            }
            else if (board[s].color() != color)
                attack(s);

            return false;
        });
        return res;
    }
}

namespace King {
    template <class F>
    void variants(int pos, F f) {
        int x = pos % 8;
        int y = pos / 8;

        for (auto i: {-1, 0, 1})
            for (auto j: {-1, 0, 1}) {
                if (i == 0 && j == 0)
                    continue;
                int nx = x + i;
                int ny = y + j;
                if (nx >= 0 && nx < 8 && ny >= 0 && ny < 8)
                    f(ny * 8 + nx);
            }
    }

    template <class S, class A>
    void steps(int pos, bool color, const std::vector<Piece>& board, S step, A attack) {
        variants(pos, [](int s) {
            if (board[s].type() == NONE) {
                step(s);
                return true;
            }
            else if (board[s].color() != color)
                attack(s);

            return false;
        });
    }
}

template <class S, class A>
void step(int move, int pos, const std::vector<int>& board, S s, A a) {
    switch (d_type) {
    case KNIGHT:
        Knight::steps(pos, board, s, a);
    case BISHOP:
        Bishop::steps(pos, board, s, a);
    case KING:
        King::steps(move, pos, board, s, a);
    }
}

class BoardModel : public QAbstractListModel {
    Q_OBJECT
    Q_PROPERTY(QString path READ getPath WRITE setPath NOTIFY changedPath)
    Q_PROPERTY(QVariantList positions READ getPositions NOTIFY changedPositions)

    QString path;
    int move;
    std::vector<Piece> board;

public:
    explicit BoardModel(QObject *parent = 0) : QAbstractListModel(parent){
        board.push_back(Piece(true, ROOK));
        board.push_back(Piece(true, KNIGHT));
        board.push_back(Piece(true, BISHOP));
        board.push_back(Piece(true, QUEEN));
        board.push_back(Piece(true, KING));
        board.push_back(Piece(true, BISHOP));
        board.push_back(Piece(true, KNIGHT));
        board.push_back(Piece(true, ROOK));
        for (int i = board.size() ; i < 16; i++)
            board.push_back(Piece(true, KING));

        for (int i = board.size() ; i < 48; i++)
            board.push_back(Piece());

        for (int i = board.size() ; i < 64; i++)
            board.push_back(Piece(false, BISHOP));
    }

    enum Roles {
        Name = Qt::DisplayRole,
        ImagePath,
    };

    QHash<int, QByteArray> roleNames() const {
        QHash<int, QByteArray> roles;
        roles[Name] = "name";
        roles[ImagePath] = "imagePath";
        return roles;
    }

    QString getPath() const {
        return path;
    }

    void setPath(QString p) {
        path = p;
    }

    QVariantList getPositions() const {
        return QVariantList();
    }

    int rowCount(const QModelIndex & = QModelIndex()) const { return 64; }

    QVariant data( const QModelIndex & index, int role = Qt::DisplayRole ) const
    {
        const Piece& pc = board[index.row()];

        if ( role == Name )
            return "WhiteKnight";
        else if ( role == ImagePath )
        {
            if (board[index.row()].type() == KNIGHT)
                return "qrc:/images/nl.svg";
            else if (board[index.row()].type() == ROOK)
                return "qrc:/images/rl.svg";
            else if (board[index.row()].type() == KING)
                return "qrc:/images/kl.svg";
            else if (board[index.row()].type() == QUEEN)
                return "qrc:/images/ql.svg";
            else if (board[index.row()].type() == BISHOP)
                return "qrc:/images/bl.svg";
            else
                return "";
        }

        return QVariant();
    }

signals:
    void changedPath();
    void changedPositions();

public slots:
    QVariant getAttack(int id) const { return QVariant(); }

};

#endif // BOARDMODEL_H
