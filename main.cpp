#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQuickItem>

#include "boardmodel.h"


int main(int argc, char *argv[]) {

    QApplication app(argc, argv);

    qmlRegisterType<BoardModel>("MyComponents", 1, 0, "BoardModel");
    QQmlApplicationEngine engine(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
