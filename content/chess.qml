import QtQuick 2.2
import QtQuick.Controls 1.1
import MyComponents 1.0

Item {
    Image {
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
        source: "qrc:/images/background.jpg"
    }

    BoardModel {
        id: chessModel
    }

//    ListModel { // should be reimplemented
//        id: chessModel
//        ListElement { imagePath: "qrc:/images/kl.svg" }
//        ListElement { imagePath: "qrc:/images/kl.svg" }
//        ListElement { imagePath: "qrc:/images/kl.svg" }
//        ListElement { imagePath: "qrc:/images/kl.svg" }
//        ListElement { imagePath: "qrc:/images/nl.svg" }
//        ListElement { imagePath: "qrc:/images/nl.svg" }
//        ListElement { imagePath: "qrc:/images/nl.svg" }
//        ListElement { imagePath: "qrc:/images/ql.svg" }
//    }

    Item {
        id: pieceContainer
        anchors.fill: parent
        z: 1000 // on top of all objects
    }

    GridView {
        id: chessGrid
        width: minHW * 2 / 3
        height: minHW * 2 / 3
        anchors.centerIn: parent
        cellWidth: width / 8
        cellHeight: height / 8
        interactive: false
        model: chessModel

        property int minHW: parent.width < parent.height ? parent.width : parent.height
        property int dragIdx: -1

        delegate: Rectangle {
            width: chessGrid.width / 8
            height: chessGrid.height / 8
            color: ((Math.floor(index / 8 + index % 2) % 2) === 1) ? "#000000" : "#FFFFFF"

            Image {
                id: itemImage
                width: chessGrid.width / 8
                height: chessGrid.width / 8
                anchors.centerIn: parent
                source: imagePath
                fillMode: Image.PreserveAspectCrop
                sourceSize.width: parent.width
                sourceSize.height: parent.width
            }

            states: [
                State {
                    name: "inDrag"
                    when: index === chessGrid.dragIdx // this defines the item for which this state turns on
                    PropertyChanges { target: itemImage; parent: pieceContainer }
                    PropertyChanges { target: itemImage; anchors.centerIn: undefined }
                    PropertyChanges { target: itemImage; x: coords.mouseX + itemImage.width * 1.5 }
                    PropertyChanges { target: itemImage; y: coords.mouseY + itemImage.height * 1.5 }
                }
            ]
        }

        MouseArea {
            id: coords
            anchors.fill: parent

            onReleased: {
                if (chessGrid.dragIdx != -1) {
                    var dragIndex = chessGrid.dragIdx
                    chessGrid.dragIdx = -1
                    chessModel.move(dragIndex,chessGrid.indexAt(mouseX, mouseY), 1)
                }
            }
            onPressed: {
                chessGrid.dragIdx = chessGrid.indexAt(mouseX, mouseY)
            }
        }
    }
}
