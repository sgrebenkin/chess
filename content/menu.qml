import QtQuick 2.2
import QtQuick.Controls 1.1

Item {
    Image {
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
        source: "qrc:/images/background.jpg"
    }

    Column {
        anchors.centerIn: parent

        Repeater {
            model: [
                { key: qsTr("Start"), link: "qrc:/content/chess.qml" },
                { key: qsTr("Load"), link: "qrc:/content/load.qml" },
                { key: qsTr("Quit"), link: "" }
            ]
            delegate: Item {
                id: dlgt
                width: 1
                height: txt.font.pixelSize + 20
                Text {
                    id: txt
                    anchors.centerIn: parent
                    color: "white"
                    text: modelData.key
                    font.pointSize: 45
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if (txt.text === "Quit") { Qt.quit() }

                            console.log(txt.link, dlgt.link, "qrc")
                            if (horizontalModel.count < 2) {
                                horizontalModel.append({page: "qrc:/content/chess.qml"})
                            }

                            horizontalView.currentIndex = horizontalModel.count - 1
                        }
                    }
                }
            }
        }
    }
}
